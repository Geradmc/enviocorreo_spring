# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
FROM java:8
VOLUME /tmp 
EXPOSE 8181-8282
ADD ./ /tmp/
ENTRYPOINT ["java","-jar","/tmp/target/EnvioCorreo-1.0.jar"]