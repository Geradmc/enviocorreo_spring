/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo;

import com.enviocorreo.models.Vendedor;
import com.enviocorreo.servicio.VendedorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gerardo
 */
@RestController
public class ControladorVendedor {
  @Autowired
  private VendedorService vendedoreservice;

  @PostMapping("/vendedores")
  public List<Vendedor> all() {
    return vendedoreservice.listarVendedores();
  }

  @PostMapping("/vendedor")
  public Vendedor newVendedor(@RequestBody Vendedor vendedor) {
    vendedoreservice.guardar(vendedor);
    return vendedor;
  }

  @PostMapping("/vendedor/{id}")
  public Vendedor getVendedor(@PathVariable Long id) {
    return vendedoreservice.encontrarVendedorID(id);
  }

  @PostMapping("/vendedor/delete/{id}")
  public String deleteVendedor(@PathVariable Long id) {
    Vendedor vendedor = vendedoreservice.encontrarVendedorID(id);
    vendedoreservice.eliminar(vendedor);
    return "Vendedor con id "+id+" a sido eliminado";
  }
  
  @PostMapping("/vendedor/editar/{id}")
  public String editarVendedor(@RequestBody Vendedor vendedor) {
    Vendedor d = vendedoreservice.encontrarVendedor(vendedor);
    vendedoreservice.guardar(vendedor);
    return "Vendedor con id "+d.getId()+" a sido actualizado";
  }
}
