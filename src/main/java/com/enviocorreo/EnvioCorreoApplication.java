package com.enviocorreo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages={"com"})
public class EnvioCorreoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnvioCorreoApplication.class, args);
	}

}
