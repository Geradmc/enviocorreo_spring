/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo;

import com.enviocorreo.models.Persona;
import com.enviocorreo.servicio.PersonaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gerardo
 */

@RestController
public class ControladorPersona {
    
  @Autowired
  private PersonaService personaservice;

  @GetMapping("/personas")
  List<Persona> all() {
    return personaservice.listarPersonas();
  }

  @PostMapping("/persona")
  Persona newPersona(@RequestBody Persona persona) {
    personaservice.guardar(persona);
    return persona;
  }

  // Single item

  @GetMapping("/persona/{id}")
  Persona one(@PathVariable Long id) {
    return personaservice.encontrarPersonabyID(id);
  }

  @PutMapping("/personas/{id}")
  Persona replaceEmployee(@RequestBody Persona persona, @PathVariable Long id) {
    Persona p1 = personaservice.encontrarPersonabyID(id);
    persona.setNombre(persona.getNombre());
    persona.setApellido(persona.getApellido());
    personaservice.guardar(persona);
    return persona;
  }

  @DeleteMapping("/persona/{id}")
  void deletePersona(@PathVariable Long id) {
    Persona persona = personaservice.encontrarPersonabyID(id);
    personaservice.eliminar(persona);
  }
}
