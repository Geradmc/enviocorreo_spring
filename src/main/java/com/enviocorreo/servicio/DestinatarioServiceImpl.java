/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.servicio;

import com.enviocorreo.dao.DestinatarioDao;
import com.enviocorreo.models.Destinatario;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gerardo
 */
@Service
public class DestinatarioServiceImpl implements DestinatarioService{
    @Autowired
    private DestinatarioDao destinatariodao;
    
    @Override
    public List<Destinatario> listarDestinatarios() {
        return (List<Destinatario>)destinatariodao.findAll();
    }

    @Override
    public void guardar(Destinatario destinatario) {
        destinatariodao.save(destinatario);
    }

    @Override
    public void eliminar(Destinatario destinatario) {
        destinatariodao.delete(destinatario);
    }

    @Override
    public Destinatario encontrarDestinatario(Destinatario destinatario) {
        return destinatariodao.findById(destinatario.getId()).orElse(null);
    }    
    
    @Override
    public Destinatario encontrarDestinatarioID(Long id) {
        return destinatariodao.findById(id).orElse(null);
    }
    
    @Override
    public List<Destinatario> encontrarDestinatariosPorTipo(String tipo){
        List<Destinatario> l = (List<Destinatario>)destinatariodao.findAll();
        List<Destinatario> result = new ArrayList();
        for(int i=0; i<l.size(); i++){
            Destinatario d = l.get(i);
            if(d.getTipo().equals(tipo)){
                result.add(d);
            }
        }
        return result;
    }
}
