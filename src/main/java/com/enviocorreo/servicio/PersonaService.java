/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.servicio;

import com.enviocorreo.models.Persona;
import java.util.List;

/**
 *
 * @author Gerardo
 */
public interface PersonaService {
    
    public List<Persona> listarPersonas();
    public void guardar(Persona persona);
    public void eliminar(Persona persona);
    public Persona encontrarPersona(Persona persona);
    public Persona encontrarPersonabyID(Long id);
}
