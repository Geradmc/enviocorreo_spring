/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.servicio;

import com.enviocorreo.models.Proceso;
import java.util.List;

/**
 *
 * @author Gerardo
 */
public interface ProcesoService {
    public List<Proceso> listarProcesos();
    public void guardar(Proceso proceso);
    public void eliminar(Proceso proceso);
    public Proceso encontrarProceso(Proceso proceso);
    public Proceso encontrarProcesoID(Long id);
    public Proceso buscarProceso(String modulo,String Proceso, Long id_vendedor);
}
