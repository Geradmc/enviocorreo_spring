/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.servicio;

import com.enviocorreo.dao.ProcesoDao;
import com.enviocorreo.models.Proceso;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gerardo
 */
@Service
public class ProcesoServiceImpl implements ProcesoService{
    @Autowired
    private ProcesoDao procesodao;
    
    @Override
    public List<Proceso> listarProcesos() {
        return (List<Proceso>)procesodao.findAll();
    }

    @Override
    public void guardar(Proceso proceso) {
        procesodao.save(proceso);
    }

    @Override
    public void eliminar(Proceso proceso) {
        procesodao.delete(proceso);
    }

    @Override
    public Proceso encontrarProceso(Proceso proceso) {
        return procesodao.findById(proceso.getId()).orElse(null);
    }    
    
    @Override
    public Proceso encontrarProcesoID(Long id) {
        return procesodao.findById(id).orElse(null);
    }
    
    public Proceso buscarProceso(String modulo,String proceso, Long id_vendedor){
        
        return procesodao.findProceso(modulo, proceso, id_vendedor);
    }
}
