/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.servicio;

import com.enviocorreo.dao.VendedorDao;
import com.enviocorreo.models.Vendedor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gerardo
 */
@Service
public class VendedorServiceImpl implements VendedorService{
    @Autowired
    private VendedorDao vendedordao;
    
    @Override
    public List<Vendedor> listarVendedores() {
        return (List<Vendedor>)vendedordao.findAll();
    }

    @Override
    public void guardar(Vendedor Vendedor) {
        vendedordao.save(Vendedor);
    }

    @Override
    public void eliminar(Vendedor Vendedor) {
        vendedordao.delete(Vendedor);
    }

    @Override
    public Vendedor encontrarVendedor(Vendedor Vendedor) {
        return vendedordao.findById(Vendedor.getId()).orElse(null);
    }    
    
    @Override
    public Vendedor encontrarVendedorID(Long id) {
        return vendedordao.findById(id).orElse(null);
    }
}
