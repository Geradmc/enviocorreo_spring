/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.servicio;

import com.enviocorreo.models.Vendedor;
import java.util.List;

/**
 *
 * @author Gerardo
 */
public interface VendedorService {
    public List<Vendedor> listarVendedores();
    public void guardar(Vendedor proceso);
    public void eliminar(Vendedor proceso);
    public Vendedor encontrarVendedor(Vendedor proceso);
    public Vendedor encontrarVendedorID(Long id);
}
