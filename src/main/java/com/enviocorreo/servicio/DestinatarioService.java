/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.servicio;

import com.enviocorreo.models.Destinatario;
import java.util.List;

/**
 *
 * @author Gerardo
 */
public interface DestinatarioService {
    public List<Destinatario> listarDestinatarios();
    public void guardar(Destinatario destinatario);
    public void eliminar(Destinatario destinatario);
    public Destinatario encontrarDestinatario(Destinatario destinatario);
    public Destinatario encontrarDestinatarioID(Long id);
    public List<Destinatario> encontrarDestinatariosPorTipo(String tipo);
}
