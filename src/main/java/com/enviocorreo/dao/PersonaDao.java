/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.dao;

import com.enviocorreo.models.Persona;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Gerardo
 */

public interface PersonaDao extends CrudRepository<Persona,Long>{
    
}
