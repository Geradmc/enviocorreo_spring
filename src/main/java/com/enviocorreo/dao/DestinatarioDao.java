/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.dao;

import com.enviocorreo.models.Destinatario;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Gerardo
 */
public interface DestinatarioDao extends CrudRepository<Destinatario,Long>{
    
}