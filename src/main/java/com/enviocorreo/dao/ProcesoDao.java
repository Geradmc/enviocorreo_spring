/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.dao;

import com.enviocorreo.models.Proceso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Gerardo
 */
public interface ProcesoDao extends CrudRepository<Proceso,Long>{
    @Query("select p from Proceso p WHERE p.modulo = :moduloName and p.proceso = :procesoName and p.vendedor_id = :vendedorId")
    public Proceso findProceso(@Param("moduloName") String moduloName,@Param("procesoName") String procesoName,@Param("vendedorId") Long vendedorId);
}
