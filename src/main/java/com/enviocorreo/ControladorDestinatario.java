/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo;

import com.enviocorreo.models.Destinatario;
import com.enviocorreo.servicio.DestinatarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gerardo
 */
@RestController
public class ControladorDestinatario {
  @Autowired
  private DestinatarioService destinatarioservice;

  @PostMapping("/destinatarios")
  public List<Destinatario> all() {
    return destinatarioservice.listarDestinatarios();
  }

  @PostMapping("/destinatario")
  public Destinatario newDestinatario(@RequestBody Destinatario destinatario) {
    destinatarioservice.guardar(destinatario);
    return destinatario;
  }

  @PostMapping("/destinatario/{id}")
  public Destinatario getDestinatario(@PathVariable Long id) {
    return destinatarioservice.encontrarDestinatarioID(id);
  }

  @PostMapping("/destinatario/delete/{id}")
  public String deleteDestinatario(@PathVariable Long id) {
    Destinatario destinatario = destinatarioservice.encontrarDestinatarioID(id);
    destinatarioservice.eliminar(destinatario);
    return "Destinatario con "+id+" a sido eliminado";
  }
  
  @PostMapping("/destinatario/editar/{id}")
  public String editarDestinatario(@RequestBody Destinatario destinatario) {
    Destinatario d = destinatarioservice.encontrarDestinatario(destinatario);
    destinatarioservice.guardar(destinatario);
    return "Destinatario con "+d.getId()+" a sido actualizado";
  }
}
