/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.configuracion;

/**
 *
 * @author Gerardo
 */
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring.datasource")
public class DBConfiguracion {
	
	private String driverClassName;
	private String url;
	private String username;
	private String password;

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Profile("dev")
	@Bean
	public String devDatabaseConnection() {
		System.out.println("Conexión por Mysql");
		System.out.println(driverClassName);
		System.out.println(url);
		return "Conexión Mysql";
	}

	@Profile("test")
	@Bean
	public String testDatabaseConnection() {
		System.out.println("Conexión Mongo");
		System.out.println(driverClassName);
		System.out.println(url);
		return "Conexión Redis";
	}

	@Profile("prod")
	@Bean
	public String prodDatabaseConnection() {
		System.out.println("Conexión Redis");
		System.out.println(driverClassName);
		System.out.println(url);
		return "Conexión MongoDB";
	}
}