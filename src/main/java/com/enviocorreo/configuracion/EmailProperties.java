/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo.configuracion;

import java.util.Properties;
import javax.mail.Session;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 *
 * @author Gerardo
 */
@Configuration
public class EmailProperties {
    
    @Bean
    public JavaMailSender primarySender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setUsername("servidorcorreo585@gmail.com");
        mailSender.setPassword("km!S.kWS7Q");
        mailSender.setPort(587);
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        
        mailSender.setJavaMailProperties(props);
        return mailSender;
    }

    @Bean
    //@ConfigurationProperties(prefix = "spring.mail.secondary")
    public JavaMailSender secondarySender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("in-v3.mailjet.com");
        mailSender.setUsername("9a699f1d1bc89f883f5147ced5a0a69c");
        mailSender.setPassword("366cc28e51500446ee05f642d76ef032");
        mailSender.setPort(587);
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        
        mailSender.setJavaMailProperties(props);
        return mailSender;
    }
}
