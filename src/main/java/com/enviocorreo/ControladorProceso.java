/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo;

import com.enviocorreo.models.Destinatario;
import com.enviocorreo.models.Proceso;
import com.enviocorreo.servicio.DestinatarioService;
import com.enviocorreo.servicio.ProcesoService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gerardo
 */
@RestController
public class ControladorProceso {
  @Autowired
  private ProcesoService procesoservice;
  @Autowired
  private DestinatarioService destinatarioservice;
  @Autowired
  private @Qualifier("primarySender")JavaMailSender sendergmail;
  @Autowired
  private @Qualifier("secondarySender")JavaMailSender senderotro;

  @PostMapping("/procesos")
  public List<Proceso> all() {
    return procesoservice.listarProcesos();
  }

  @PostMapping("/proceso")
  public Proceso newProceso(@RequestBody Proceso proceso) {
    procesoservice.guardar(proceso);
    return proceso;
  }

  @PostMapping("/proceso/{id}")
  public Proceso getProceso(@PathVariable Long id) {
    return procesoservice.encontrarProcesoID(id);
  }

  @PostMapping("/proceso/delete/{id}")
  public String deleteProceso(@PathVariable Long id) {
    Proceso proceso = procesoservice.encontrarProcesoID(id);
    procesoservice.eliminar(proceso);
    return "Proceso con id "+id+" a sido eliminado";
  }
  
  @PostMapping("/proceso/editar/{id}")
  public String editarProceso(@RequestBody Proceso proceso) {
    Proceso d = procesoservice.encontrarProceso(proceso);
    procesoservice.guardar(proceso);
    return "Proceso con id "+d.getId()+" a sido actualizado";
  }
  
  @PostMapping("/enviarcorreo")
  public String ejecutarProceso(@RequestBody Map<String, String> data){
      int i;
      String modulo          = "";
      String proceso         = "";
      Long id_vendedor       = 0L;
      String replyto         = "";
      String correoadicional = "";
      Long id_destinatario   = 0L;
      int incluir_copia      = 0;
      int servidor_correo    = 0;
      
      for (Map.Entry entry : data.entrySet()){
        if(entry.getKey()=="modulo"){
            modulo = (String)entry.getValue();
        }
        if(entry.getKey()=="proceso"){
            proceso = (String)entry.getValue();
        }
        if(entry.getKey()=="id_vendedor"){
            id_vendedor = Long.parseLong((String)entry.getValue());
        }
        if(entry.getKey()=="replyto"){
            replyto = (String)entry.getValue();
        }
        if(entry.getKey()=="email_adicional"){
            correoadicional = (String)entry.getValue();
        }
        if(entry.getKey()=="servidor"){
            servidor_correo = Integer.parseInt((String)entry.getValue());
        }
        if(entry.getKey()=="destinatario"){
            id_destinatario = Long.parseLong((String)entry.getValue());
        }
        if(entry.getKey()=="incluir_copia"){
            incluir_copia = Integer.parseInt((String)entry.getValue());
        }
      }
      
      Proceso p              = procesoservice.buscarProceso(modulo, proceso, id_vendedor);
      String resultado;
      if(p!=null){
        String[] destinatarios = p.getDestinatarios_id().split(",");
        Destinatario dest=null;
        for(i=0; i<destinatarios.length; i++){
          Long id_d = Long.parseLong(destinatarios[i]);
          if(id_d==id_destinatario){
             dest = destinatarioservice.encontrarDestinatarioID(id_d);
             break;
          }
        }
        
        List<Destinatario> ld  = new ArrayList();
        if(incluir_copia==1){
            ld = destinatarioservice.encontrarDestinatariosPorTipo("CC");
        }
        
        if(dest!=null){
            resultado = sendEmail(ld,replyto,correoadicional,servidor_correo,p,dest);
        } else{
            resultado = "El destinatario seleccionado no se encuentra en la lista del vendedor";
        }
      } else{
          resultado="No se pudo enviar el correo, el vendedor no cuenta con el proceso seleccionado";
      }
      
      return resultado;
  }
  public String sendEmail(List<Destinatario> destinatarios,String replyto,String correoadicional,int servidor, Proceso p, Destinatario destinatario){
      SimpleMailMessage msg = new SimpleMailMessage();
      int numcorreos_cc=destinatarios.size();
      String msg_result="";
      
      if(!correoadicional.equals("")){
        numcorreos_cc++;
      }
      String[] correos = new String[numcorreos_cc];
      for(int i=0; i<destinatarios.size(); i++){
        correos[i] = destinatarios.get(i).getEmail();
      }
      if(!correoadicional.equals("")){
        correos[numcorreos_cc-1]=correoadicional;
      }
      msg.setCc(correos);
      if(!replyto.equals("")){
          msg.setReplyTo(replyto);
      }
      msg.setTo(destinatario.getEmail());
      
      msg.setSubject(getAsunto(p.getProceso()));
      msg.setText(crearMensaje(p,destinatario));
      
      if(servidor==1){
          sendergmail.send(msg);
          msg_result="Correo enviado por GMAIL. Correos enviados correctamente";
      } else if(servidor==2){
          msg.setFrom("gerardomauricio85@gmail.com");
          senderotro.send(msg);
          msg_result="Correo enviado por mailjet. Correos enviados correctamente";
      } else{
          return "No se eligio servidor de correo. opcion 1: gmail, opcion 2: mailjet";
      }
      
      return msg_result;
  }
  
  public String getAsunto(String proceso){
      String asunto=proceso;
      return asunto;
  }
  
  public String crearMensaje(Proceso p,Destinatario d){
      String mensaje = "Estimado Usuario "+d.getNombre()+"\n"
                      +"Se le comunica que la solicitud de "+p.getProceso()+" de "+p.getModulo()+"  esta en proceso.\n"
                      +"Se le estara informando por este medio.\n"
                      +"\n"
                      +"Saludos Cordiales\n";
      return mensaje;
  }
}
