/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enviocorreo;

import com.enviocorreo.dao.PersonaDao;
import com.enviocorreo.models.Destinatario;
import com.enviocorreo.models.Persona;
import com.enviocorreo.servicio.DestinatarioService;
import com.enviocorreo.servicio.PersonaService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gerardo
 */
@Controller
@Slf4j
public class Controlador {
    
    @Autowired
    private PersonaService personaservice;
    
    @Autowired
    private DestinatarioService destinatarioservice;
    
    @GetMapping("/")
    public String inicio(Model model){
        List<Persona> personas = personaservice.listarPersonas();
        System.out.print(personas);
        model.addAttribute("personas",personas);
        return "index";
    }
    
    @GetMapping("/agregar")
    public String agregar(Persona persona){
        return "modificar";
    }
    
    @PostMapping("/guardar")
    public String guardar(Persona persona){
        personaservice.guardar(persona);
        return "redirect:/";
    }
}
